/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package camera;

public abstract class DigitalCamera {
    protected String make;
    protected String model;
    protected double megaPixel;
    protected double internalsize;
    protected double externalsize;

    public DigitalCamera(String make, String model, double megaPixel, double internalsize, double externalsize) {
        this.make = make;
        this.model = model;
        this.megaPixel = megaPixel;
        this.internalsize = internalsize;
        this.externalsize = externalsize;
    }
    
    public abstract String describeCamera();
    
}

