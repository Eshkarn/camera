/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package camera;

public class PointAndShoot extends DigitalCamera{
    
    public PointAndShoot(String make, String model, double megaPixel, double internalsize, double externalsize) {
        super(make, model, megaPixel, internalsize, externalsize);
    }
  

    @Override
    public String describeCamera() {
       String desc= this.make + "\n " + this.model + "\n " + this.megaPixel + "\n " + this.internalsize + "\n " + this.externalsize;
    return desc;
    }
}

